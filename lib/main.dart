import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sudoku_app/screens/instructions.dart';
import 'package:sudoku_app/sudoku_notifier.dart';
import 'localizations_delegate.dart';
import 'localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'screens/home.dart';
import 'screens/game.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => SudokuChangeNotifier(),
      child: MyApp(),
    ),
  );
}


class MyApp extends StatelessWidget {

  final String appTitle = 'Sudoku';
  Locale locale;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context, 'title'),
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        // GlobalCupertinoLocalizations.delegate, <- for ios
      ],
      supportedLocales: [
        const Locale('en', ''), // English
        const Locale('no', ''), // Norwegian
      ],
      // sets norwegian to default language
      localeResolutionCallback: (deviceLocale, supportedLocales) {
        if (this.locale == null) {
          this.locale = deviceLocale.languageCode.compareTo('no') == 0
              ? supportedLocales.first
              : supportedLocales.last;
        }
        return this.locale;
      },
      home: HomeScreen(title: appTitle),
      initialRoute: '/',
      routes: {
        //GameScreen.routeName: (context) => GameScreen(),
        '/instructions': (context) => Instructions(),
        '/play': (context) => Game(),
      },
    );
  }
}
