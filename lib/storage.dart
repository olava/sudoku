import 'dart:io';
import 'package:path_provider/path_provider.dart';

class Storage {

  Future<String> get _localPath async {
    // use this to store files in documents directory on device (iOS: NSDocumentDirectory / Android: AppData)
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/board.txt');
  }

  Future<String> readSudokuBoard() async {
    try {
      final file = await _localFile;
      String contents = await file.readAsString();
      print(contents);
      //return int.parse(contents);
      return contents;
    } catch (e) {
      return '';
    }
  }

  Future<File> writeSudokuBoard(String board) async {
    final file = await _localFile;
    return file.writeAsString('$board');
  }
}