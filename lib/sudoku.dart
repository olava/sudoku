import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sudoku_app/localizations.dart';
import 'package:sudoku_app/sudoku_notifier.dart';
import 'package:sudoku_app/storage.dart';
import 'package:tuple/tuple.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

class SudokuBoard extends StatefulWidget {
  final String boardSize, difficulty;
  final Storage storage;
  SudokuBoard({Key key, @required this.boardSize, @required this.difficulty, @required this.storage}) : super(key: key);

  @override
  _SudokuBoardState createState() => _SudokuBoardState(boardSize, difficulty, storage);
}

class _SudokuBoardState extends State<SudokuBoard> {
  String boardSize, difficulty;
  Storage storage;

  _SudokuBoardState(boardSize, difficulty, storage) {
    this.boardSize = boardSize;
    this.difficulty = difficulty;
    this.storage = storage;
  }

  Color borderColor = Color(0xFFFFFF); // transparent
  int divisor, remainder, axisCount;
  List<List<int>> startBoard;
  

  List<List<int>> easy6 = [
    [0, 0, 0, 1, 0, 6],
    [6, 0, 4, 0, 0, 0],
    [1, 0, 2, 0, 0, 0],
    [0, 0, 0, 5, 0, 1],
    [0, 0, 0, 6, 0, 3],
    [5, 0, 6, 0, 0, 0],
  ];

  List<List<int>> medium6 = [
    [0, 0, 6, 0, 0, 0],
    [0, 0, 1, 0, 7, 0],
    [0, 9, 2, 3, 0, 1],
    [6, 0, 5, 0, 3, 7],
    [0, 0, 8, 2, 0, 5],
    [4, 0, 0, 1, 6, 0],
  ];

  List<List<int>> hard6 = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 6, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
  ];

  List<List<int>> easy9 = [
    [5, 3, 4, 6, 7, 8, 9, 1, 2],
    [6, 7, 2, 1, 9, 5, 3, 4, 8],
    [1, 9, 8, 3, 4, 2, 5, 6, 7],
    [8, 5, 9, 7, 6, 1, 4, 2, 3],
    [4, 2, 6, 8, 5, 3, 7, 9, 1],
    [7, 1, 3, 9, 2, 4, 8, 5, 6],
    [9, 6, 1, 5, 3, 7, 2, 8, 4],
    [2, 8, 7, 4, 1, 9, 6, 3, 5],
    [3, 4, 5, 2, 8, 6, 1, 7, 0],
  ];


  List<List<int>> medium9 = [
    [0, 0, 6, 0, 0, 0, 4, 1, 0],
    [0, 0, 1, 0, 7, 0, 0, 2, 9],
    [0, 9, 2, 3, 0, 1, 0, 0, 0],
    [6, 0, 5, 0, 3, 7, 0, 0, 2],
    [0, 0, 8, 2, 0, 5, 1, 0, 0],
    [4, 0, 0, 1, 6, 0, 5, 0, 7],
    [0, 0, 0, 9, 0, 3, 7, 6, 0],
    [9, 8, 0, 0, 1, 0, 3, 0, 0],
    [0, 6, 3, 0, 0, 0, 2, 0, 0],
  ];

  List<List<int>> hard9 = [
    [0, 0, 8, 0, 6, 0, 0, 0, 0],
    [0, 0, 9, 0, 0, 3, 0, 0, 7],
    [0, 1, 0, 0, 0, 8, 0, 9, 0],
    [0, 7, 0, 4, 5, 0, 0, 1, 0],
    [0, 3, 1, 0, 0, 0, 9, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [6, 0, 0, 0, 0, 0, 7, 0, 0],
    [0, 0, 0, 0, 3, 4, 0, 2, 6],
    [0, 0, 5, 0, 2, 0, 0, 0, 3],

  ];



  @override
  void initState() {
    setState(() {
      startBoard = easy9;
    });

    if (boardSize == '6x6') {
      divisor = 2;
      remainder = 1;
      axisCount = 6;
    } else { // 9x9
      divisor = 3;
      remainder = 2;
      axisCount = 9;
    }

    switch(difficulty) {
      case 'easy':
        startBoard = (boardSize == '6x6') ? easy6 : easy9;
        break;
      case 'medium':
        startBoard = (boardSize == '6x6') ? medium6 : medium9;
        break;
      case 'hard':
        startBoard = (boardSize == '6x6') ? hard6 : hard9;
        break;
    }
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setCellValues());
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: _getTableRows(),
      ),
    );
  }

  void setCellValues() {
    List<int> row;
    for (int i = 0; i < axisCount; i++) {
      for (int j = 0; j < axisCount; j++ ) {
        row = startBoard[i];
        context.read<SudokuChangeNotifier>().setStarterCell(i, j, row[j]);
      }
    }
    context.read<SudokuChangeNotifier>().setStartBoard();
  }


  List<TableRow> _getTableRows() {
    return List.generate(axisCount, (int rowNumber) {
      return TableRow(children: _getRow(rowNumber));
    });
  }

  List<Widget> _getRow(int rowNumber) {
    return List.generate(axisCount, (int colNumber) {
      return Container(
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(
                width: (colNumber % 3 == 2) ? 3.0 : 1.0,
                color: borderColor
            ),
            bottom: BorderSide(
                width: (rowNumber % divisor == remainder) ? 3.0 : 1.0,
                color: borderColor
            ),
          ),
        ),
        child: SudokuCell(rowNumber, colNumber),
      );
    });
  }
}

class SudokuCell extends StatelessWidget {
  final int row, col;

  SudokuCell(this.row, this.col);

  @override
  Widget build(BuildContext context) {
    return InkResponse(
      enableFeedback: true,
      onTap: () {
        Provider.of<SudokuChangeNotifier>(context, listen: false)
            .setBoardCell(this.row, this.col);
        },
      child: SizedBox(
        height: 40,
        child: Selector<SudokuChangeNotifier, Tuple2<String, Color>>(
                builder: (context, data, child) {
                  return Container (
                      color: data.item2,
                      child: Center(
                          child: Text(data.item1)
                      )
                  );
                  },
                selector: (context, sudokuChangeNotifier) => Tuple2 (
                    sudokuChangeNotifier.getBoardCell(this.row, this.col),
                    sudokuChangeNotifier.getBoardCellColor(this.row, this.col)
                )
            ),
      ),
    );
  }
}


