import 'package:flutter/material.dart';
import 'package:sudoku_app/components/toolbar.dart';
import 'package:sudoku_app/localizations.dart';
import 'package:sudoku_app/sudoku.dart';
import 'package:sudoku_app/storage.dart';
import 'package:sudoku_app/sudoku_notifier.dart';

class Game extends StatelessWidget {
  final boardSize, difficulty;
  Game({Key key, this.boardSize, this.difficulty}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal[200],
      appBar: AppBar(
        title: Text(AppLocalizations.of(context, 'title')),
      ),
      body: Column(
          children: [
            SudokuBoard(boardSize: boardSize, difficulty: difficulty, storage: Storage()),
            Toolbar(range: (boardSize == '9x9' ? 9 : 6)),
            // Icon(Icons.done)
          ],
        ),
    );
  }
}
