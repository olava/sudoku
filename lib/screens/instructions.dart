import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sudoku_app/localizations.dart';

class Instructions extends StatefulWidget {

  Instructions({Key key}) : super(key: key);

  @override
  _InstructionsState createState() => _InstructionsState();

}

class _InstructionsState extends State<Instructions> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context, 'instructions')),
      ),
      body: Center(
          child: Text(AppLocalizations.of(context, 'howToPlay')),
      ),
    );
  }
}