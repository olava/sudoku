import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:sudoku_app/localizations.dart';
import 'package:sudoku_app/screens/game.dart';
import 'package:sudoku_app/components/button.dart';
import 'package:sudoku_app/sudoku_notifier.dart';

class HomeScreen extends StatefulWidget {
  final String title;
  HomeScreen({Key key, this.title}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String difficulty, boardSize;

  @override
  void initState() {
    difficulty = 'medium';
    boardSize = '9x9';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.info),
              title: Text(AppLocalizations.of(context, 'instructions')),
                  onTap: () {
                Navigator.pushNamed(context, '/instructions');
            },
            ),
          ],
        ),
      ),

      appBar: AppBar(
        title: Text(AppLocalizations.of(context, 'title')),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),

      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              playButton(),
              Column(
                children: [
                  boardSizeButtonBar(),
                  difficultyButtonBar(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget playButton() => Container(
    child: RaisedButton(onPressed: () {
      print(this.difficulty + this.boardSize);
      Navigator.push(context, MaterialPageRoute(builder: (context) =>
          Game(boardSize: boardSize, difficulty: difficulty,))
      );
    },
      child: Text(AppLocalizations.of(context, 'play')),
    ),
  );

  Widget boardSizeButtonBar() => Container(
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
            padding: const EdgeInsets.only(left: 4.0, right: 4.0),
            child: boardButton('9x9')
        ),
        Container(
            padding: const EdgeInsets.only(left: 4.0, right: 4.0),
            child: boardButton('6x6')
        ),
      ],
    ),
  );

  Widget difficultyButtonBar() => Container(
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: const EdgeInsets.only(left: 4.0, right: 4.0),
          child: difficultyButton('easy'),
        ),
        Container(
            padding: const EdgeInsets.only(left: 4.0, right: 4.0),
            child: difficultyButton('medium')
        ),
        Container(
            padding: const EdgeInsets.only(left: 4.0, right: 4.0),
            child: difficultyButton('hard')
        ),
      ],
    ),
  );

  Widget boardButton(String label) => Button(
    key: ValueKey('button$label'),
    isSelected: this.boardSize == label,
    label: AppLocalizations.of(context, label),
    onPressed: () {
      //TODO add warning before clearing ongoing game
      context.read<SudokuChangeNotifier>().clearBoard();
      setState(() {
        this.boardSize = label;
      });
    },
  );

  Widget difficultyButton(String label) => Button(
    key: ValueKey('button$label'),
    isSelected: this.difficulty == label,
    label: AppLocalizations.of(context, label),
    onPressed: () {
      //TODO add warning before clearing ongoing game
      context.read<SudokuChangeNotifier>().clearBoard();
      setState(() {
        this.difficulty = label;
      });
    },
  );

}




