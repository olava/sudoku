import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sudoku_app/sudoku_notifier.dart';
import 'button.dart';

class NumberScrollbar extends StatefulWidget {
  final int numberRange;
  NumberScrollbar({Key key, @required this.numberRange}) : super(key: key);

  @override
  _NumberScrollbarState createState() => _NumberScrollbarState(numberRange);
}

class _NumberScrollbarState extends State<NumberScrollbar> {
  int numberRange;

  _NumberScrollbarState(numberRange) {
    this.numberRange = numberRange;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                for (int i = 0; i < numberRange; i++)
                  Container(
                      padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                      //TODO add button to remove numbers on the board
                      child: button((i + 1))
                  ),
              ],
            ),
          ),
      );
  }

  Widget button(int label) => Button(
    key: ValueKey('button$label'),
    isSelected: context.watch<SudokuChangeNotifier>().activeNumber == label,
    label: label.toString(),
    onPressed: () {
      context.read<SudokuChangeNotifier>().setActiveNumber(label);
    },
  );

}