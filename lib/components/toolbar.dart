import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../localizations.dart';
import '../sudoku_notifier.dart';
import 'number_scrollbar.dart';

class Toolbar extends StatefulWidget {
  final int range;
  Toolbar({Key key, this.range}) : super(key: key);

  @override
  _ToolbarState createState() => _ToolbarState(range);
}

class _ToolbarState extends State<Toolbar> {
  int numberRange;

  _ToolbarState(numberRange) {
    this.numberRange = numberRange;
  }

  List<bool> isSelected = List.generate(3, (_) => false);
  bool numbersVisible = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [

          Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ToggleButtons(
                    children: <Widget>[
                      Icon(Icons.format_color_fill),
                      Icon(Icons.filter_1),
                      Icon(Icons.autorenew),
                    ],
                    color: Colors.green,
                    selectedColor: Colors.orange,
                    fillColor: Colors.yellow,
                    onPressed: (int index) {
                      tapCallback(index);
                    },
                    isSelected: isSelected,
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: numbersVisible,
            child: Container(child: NumberScrollbar(numberRange: numberRange)),
          ),
        ],
      ),
    );
  }

  void tapCallback(int index) {
    setState(() {
      for (int buttonIndex = 0; buttonIndex < isSelected.length; buttonIndex++) {
        if (buttonIndex == index) {
          isSelected[buttonIndex] = !isSelected[buttonIndex];
        } else {
          isSelected[buttonIndex] = false;
        }
      }
      context.read<SudokuChangeNotifier>().setActiveTool(isSelected[index] ? index : null);
      numbersVisible = isSelected[1] ? !numbersVisible : false;

      if (isSelected[2]) {
        showResetDialog();
        isSelected[2] = !isSelected[2];
      }
    });
  }

  Future<void> showResetDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context, 'alertTitleReset')),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(AppLocalizations.of(context, 'resetMessage')),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context, 'cancelButton')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context, 'approveButton')),
              onPressed: () {
                context.read<SudokuChangeNotifier>().clearBoard();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
