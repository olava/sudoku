import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final Key key;
  final bool isSelected;
  final String label;
  final VoidCallback onPressed;

  Button({
    this.key,
    this.isSelected,
    this.label,
    this.onPressed,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: isSelected ? Colors.grey : Colors.grey[800],
        ),
        child: Text(label),
        onPressed: () {
          onPressed();
        });
  }
}