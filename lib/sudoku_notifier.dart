import 'package:flutter/material.dart';
import 'package:sudoku_app/sudoku.dart';

class SudokuChangeNotifier with ChangeNotifier {

  List<List<int>> board = List.generate(9, (_) => List.generate(9, (_) => 0));
  List<List<int>> startBoard = List.generate(9, (_) => List.generate(9, (_) => 0));

  List<List<Color>> sudokuCellColors = List.generate(9, (_) =>
      List.generate(9, (_) => Colors.white));

  int axisCount;

  int activeTool;
  int activeNumber = 0;

  bool isValid = true;

  Color primaryCellColor = Colors.white;
  Color secondaryCellColor = Colors.deepOrangeAccent[100];
  Color accentColor = Colors.grey[200];

  void setAxisCount(int count) {
    this.axisCount = count;
    this.board = List.generate(9, (_) => List.generate(axisCount, (_) => 0));
  }

  void setStartBoard() {
    startBoard = board;
  }

  String getStarterCell(int row, col) => this.board[row][col].toString();

  void setStarterCell(int row, col, value) {
    this.board[row][col] = value;
    this.sudokuCellColors[row][col] = value != 0 ? accentColor : primaryCellColor;
    notifyListeners();
  }

  String getBoardCell(int row, col) =>
      this.board[row][col] == 0 ? '' : this.board[row][col].toString();

  void setBoardCell(int row, col) {
    switch (activeTool) {
      case 0: // Color fill
      //this.sudokuCellColors[row][col] = getBoardCellColor(row, col) == secondaryCellColor ? primaryCellColor : secondaryCellColor;
      if (getBoardCellColor(row, col) == secondaryCellColor) {
        this.sudokuCellColors[row][col] = primaryCellColor;
      }
      if (getBoardCellColor(row, col) == primaryCellColor) {
        this.sudokuCellColors[row][col] = secondaryCellColor;
      }
        break;
      case 1: // Number scrollbar
      print(getStarterCell(row, col));
        if (getBoardCellColor(row, col) != accentColor) {
          this.board[row][col] = this.activeNumber;
        }
        break;
      case 2: // Reset board
        break;
    }
    this.isValid = this.isBoardValid();
    print(this.isBoardValid());
    notifyListeners(); // rebuild SudokuCell
  }

  void clearBoard() {
    for(int i = 0; i < board.length; i++) {
      for(int j = 0; j < board.length; j++) {
        this.board[i][j] = 0;
        this.sudokuCellColors[i][j] = primaryCellColor;
      }
    }
    //this.board = List.generate(9, (_) => List.generate(axisCount, (_) => 0));
    notifyListeners();
  }

  Color getBoardCellColor(int row, col) => this.sudokuCellColors[row][col];

  int getActiveTool() => this.activeTool;

  void setActiveTool(int tool) {
    this.activeTool = tool;
    notifyListeners();
  }

  int getActiveNumber() => this.activeNumber;

  void setActiveNumber(int number) {
    this.activeNumber = number;
    notifyListeners();
  }

  bool isBoardValid() {
    for (int row = 0; row < 9; row++) {
      for (int col = 0; col < 9; col++) {
        if(!(isRowValid(row) && isColumnValid(col) && isRegionValid(row, col)))
          return false;
      }
    }
    return true;
  }

  bool isRowValid(int row) =>
      this.areDuplicatesPresent(this.board[row].toList(growable: false));

  bool isColumnValid(int col) =>
      this.areDuplicatesPresent(this.board.map((r) =>
      r[col]).toList(growable: false));

  bool isRegionValid(int row, int col) {
    List<int> region = List<int>();
    int xRegion = row ~/ 3;
    int yRegion = col ~/ 3;
    for (int x = xRegion * 3; x < xRegion * 3 + 3; x++) {
      for (int y = yRegion * 3; y < yRegion * 3 + 3; y++) {
        region.add(this.board[x][y]);
      }
    }
    return this.areDuplicatesPresent(region);
  }


  bool areDuplicatesPresent(List<int> a) {
    a.sort();
    for (int i = 1; i < a.length; i++) if (a[i] != 0 && a[i - 1] == a[i])
      return false;
    return true;
  }
}