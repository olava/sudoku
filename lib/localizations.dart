import 'dart:async';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';


class AppLocalizations {
  AppLocalizations(this.locale);

  final Locale locale;

  static String of(BuildContext context, String key) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations).translate(key);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'title': 'Sudoku',
      'play': 'Play Game',
      'instructions': 'Instructions',
      'settings': 'Settings',
      'difficulty': 'Difficulty',
      'easy': 'Easy',
      'medium': 'Medium',
      'hard': 'Hard',
      '9x9': '9x9',
      '6x6': '6x6',
      'howToPlay': 'How to play',
      'fillTool': 'Fill a cell with color',
      'numberTool': 'Select a number',
      'alertTitleReset': 'Reset board',
      'resetMessage': 'Are you sure you want to start over? All filled values will be removed.',
      'approveButton': 'Yes',
      'cancelButton': 'Cancel',
      'toolMessage': 'Choose the tool you want to use',
      'win': 'Winning!!',
      'loose': 'Not correct',
    },
    'no': {
      'title': 'Sudoku',
      'play': 'Spill',
      'instructions': 'Instruksjoner',
      'settings': 'Innstillinger',
      'difficulty': 'Vanskelighetsgrad',
      'easy': 'Lett',
      'medium': 'Middels',
      'hard': 'Vanskelig',
      '9x9': '9x9',
      '6x6': '6x6',
      'howToPlay': 'Slik fungerer spillet',
      'fillTool': 'Sett farge på en celle',
      'numberTool': 'Velg et tall',
      'alertTitleReset': 'Nullstille brettet',
      'resetMessage': 'Er du sikker på at du vil starte spillet på nytt? Alle utfylte verdier vil fjernes.',
      'approveButton': 'Ok',
      'cancelButton': 'Oi, nei!',
      'toolMessage': 'Velg det verktøyet du ønsker å bruke',
      'win': 'Du fikk det til!',
      'loose': 'Det ble ikke riktig',
    },
  };

  String translate(key) => _localizedValues[locale.languageCode][key];

}
